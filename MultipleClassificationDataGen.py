import rosbag
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle

files = ["NPS_Case2_H_D70-A30-LF.bag"]

for n in np.arange(len(files)):
    # Check First Data Gen
    file_name = files[n]
    store_exists = "MeanStore." + file_name[:-4]
    if not os.path.isfile(store_exists):
        # File Update
        print(files[n])
        path = '/media/dberezansky/daniel_data/NPS_Case2/' + files[n]
        bag = rosbag.Bag(open(path, 'rb'))

        # Frame Counter
        frame_count = 0
        for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
            if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
                frame_count += 1

        # Initialize Data Set
        data_set = {}
        data = [int(foo) for foo in message.intensities] # Gets matrix of message intensities.
        data = np.reshape(data, (-1,256)) # Reshapes matrix to n columns.
        nrows, ncols = data.shape
        for r in np.arange(nrows):
            for c in np.arange(ncols):
                data_set[(r, c)] = np.zeros(400)

        # Generate Data Set
        frame_index = 0
        for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
            if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
                data = [int(foo) for foo in message.intensities] # Gets matrix of message intensities.
                data = np.reshape(data, (-1,256)) # Reshapes matrix to n columns.
                for r in np.arange(nrows):
                    for c in np.arange(ncols):
                        if frame_index > 400:
                            break
                        data_set[(r, c)][frame_index] = data[r][c]
                frame_index += 1
        print("Bag Files Loaded!\n")

        # Program progress bar
        # progress_points = 100
        # for p in np.arange(progress_points - 1):
        #     print("-", end="")
        # print("E")
        # progress_bar = 0

        # Initialize Result Matrices
        means = np.zeros(ncols * nrows).reshape((nrows, ncols)) # Initializes means in shape of intensities matrix.
        sigmas = np.zeros(ncols * nrows).reshape((nrows, ncols)) # Initializes sigmas in shape of intensities matrix.

        # Compute Results
        for r in np.arange(nrows): # Goes through each range (by row index).
            for c in np.arange(ncols): # Goes through each azimuth (by column index).
                means[r][c] = np.mean(data_set[(r,c)]) # Takes mean at given pixel point.
                sigmas[r][c] = np.std(data_set[(r,c)]) # Takes standard deviation at given pixel point.
            # if r > progress_bar * (nrows / progress_points):
            #     print("-", end="")
            #     progress_bar += 1
            # progress = str(r) + "/" + str(nrows - 1)
            # print(progress)

        # Data Serialization
        meansName = 'MeanStore.' + file_name[:-4]
        meansFile = open(meansName, 'wb')
        pickle.dump(means, meansFile)
        meansFile.close()

        sigmasName = 'SigmaStore.' + file_name[:-4]
        sigmasFile = open(sigmasName, 'wb')
        pickle.dump(sigmas, sigmasFile)
        sigmasFile.close()

    # Success Output
    exit_output = "Success! - " + files[n] + " - " + str(n + 1) + "/" + str(len(files)) + "\n\n"
    print(exit_output)
