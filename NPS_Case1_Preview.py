# Program to preview a camera and sonar image for each NPS Case 2 data set of a set frequency.
import rosbag
import numpy as np
import math
import matplotlib.pyplot as plt
import cv_bridge
import sys

frequency = sys.argv[1]
file_name = '/media/dberezansky/daniel_data/NPS_Case1/NPS_Case1_' + frequency + '.bag'
bag = rosbag.Bag(open(file_name, 'rb'))
time_factor = 1000 * 1000 * 1000

for camera_topic, camera_message, camera_timestamp in bag.read_messages():
    if camera_topic == '/raven/stereo/right/image_raw':
        br = cv_bridge.CvBridge()
        camera_data = br.imgmsg_to_cv2(camera_message, "mono8")
        break

# Plot Shape
pcols = 8
prows = 3
fig1, ax = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)
fig2, ax2 = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)
fig3, ax3 = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)
fig4, ax4 = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)
fig5, ax5 = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)

if frequency == 'HF':
    frequency_state = 'High'
    num_sonar_images = 1044
    dt = 104
elif frequency == 'LF':
    frequency_state = 'Low'
    num_sonar_images = 978
    dt = 97
else:
    frequency_state = 'Error'
    print("Frequency incorrectly defined!")

title = frequency_state + " Frequency Bagfile contains {} images and lasts {} seconds".format(num_sonar_images, dt)
fig1.suptitle(title, fontsize=20)
fig2.suptitle(title, fontsize=20)
fig3.suptitle(title, fontsize=20)
fig4.suptitle(title, fontsize=20)
fig5.suptitle(title, fontsize=20)

total = 0
count = 0
status = 0
secondary = 0

# For eaach message in bag
for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
    if count == 0:
        init = timestamp

    if topic == '/raven/stereo/right/image_raw':
        br = cv_bridge.CvBridge()
        camera_data = br.imgmsg_to_cv2(message, "mono8")
    elif topic == '/raven/oculus/sonar_image' and status == 0:
        print(count)
        if count % 12 == 0:
            sonar_plot_count = 0
            camera_plot_count = 1
        
        x_s = math.floor(sonar_plot_count / pcols)
        y_s = sonar_plot_count % pcols
        x_c = math.floor(camera_plot_count / pcols)
        y_c = camera_plot_count % pcols

        # Data gathering
        data = [int(foo) for foo in message.intensities]
        data = np.reshape(data, (-1,256))

        # Subplot label creation
        angles = [np.degrees(rad) for rad in message.azimuth_angles]
        label = "Frame " + str(total)
        label2 = "at " + str((timestamp - init) / time_factor) + " s"

        if count < 12:
            # Add Sonar and Camera Images
            ax[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")
            ax[x_c, y_c].imshow(camera_data, cmap="gray")

            # Label Subplot
            # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
            # ax[x_s, y_s].set_ylabel('Range (meters)')

            ax[x_c, y_c].axis('off')
            ax[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
            ax[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
            ax[x_s, y_s].set_aspect("auto")
            ax[x_c, y_c].set_aspect("auto")

            ax[x_s, y_s].set_title(label, fontsize=18)
            ax[x_c, y_c].set_title(label2, fontsize=18)
        elif count < 24:
            # Add Sonar and Camera Images
            ax2[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")
            ax2[x_c, y_c].imshow(camera_data, cmap="gray")

            # Label Subplot
            # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
            # ax[x_s, y_s].set_ylabel('Range (meters)')

            ax2[x_c, y_c].axis('off')
            ax2[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
            ax2[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
            ax2[x_s, y_s].set_aspect("auto")
            ax2[x_c, y_c].set_aspect("auto")

            ax2[x_s, y_s].set_title(label, fontsize=18)
            ax2[x_c, y_c].set_title(label2, fontsize=18)
        elif count < 36:
            # Add Sonar and Camera Images
            ax3[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")
            ax3[x_c, y_c].imshow(camera_data, cmap="gray")

            # Label Subplot
            # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
            # ax[x_s, y_s].set_ylabel('Range (meters)')

            ax3[x_c, y_c].axis('off')
            ax3[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
            ax3[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
            ax3[x_s, y_s].set_aspect("auto")
            ax3[x_c, y_c].set_aspect("auto")

            ax3[x_s, y_s].set_title(label, fontsize=18)
            ax3[x_c, y_c].set_title(label2, fontsize=18)
        elif count < 48:
            # Add Sonar and Camera Images
            ax4[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")
            ax4[x_c, y_c].imshow(camera_data, cmap="gray")

            # Label Subplot
            # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
            # ax[x_s, y_s].set_ylabel('Range (meters)')

            ax4[x_c, y_c].axis('off')
            ax4[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
            ax4[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
            ax4[x_s, y_s].set_aspect("auto")
            ax4[x_c, y_c].set_aspect("auto")

            ax4[x_s, y_s].set_title(label, fontsize=18)
            ax4[x_c, y_c].set_title(label2, fontsize=18)
        elif count < 60:
            # Add Sonar and Camera Images
            ax5[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")
            ax5[x_c, y_c].imshow(camera_data, cmap="gray")

            # Label Subplot
            # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
            # ax[x_s, y_s].set_ylabel('Range (meters)')

            ax5[x_c, y_c].axis('off')
            ax5[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
            ax5[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
            ax5[x_s, y_s].set_aspect("auto")
            ax5[x_c, y_c].set_aspect("auto")

            ax5[x_s, y_s].set_title(label, fontsize=18)
            ax5[x_c, y_c].set_title(label2, fontsize=18)
        else:
            break
        
        # Update Subplot
        count += 1
        total += 1
        sonar_plot_count += 2
        camera_plot_count += 2
        status = 1
    elif topic == '/raven/oculus/sonar_image' and status == 1:
        if secondary == 16:
            status = 0
            secondary = 0
            continue
        secondary += 1
        total += 1

print(total)

# Save Plot
save_title = "NPS_Case1_Preview-" + frequency 
fig1.savefig(save_title + "-1.png")
fig2.savefig(save_title + "-2.png")
fig3.savefig(save_title + "-3.png")
fig4.savefig(save_title + "-4.png")
fig5.savefig(save_title + "-5.png")
