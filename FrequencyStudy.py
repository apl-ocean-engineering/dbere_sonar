# Program to preview a camera and sonar image for each NPS Case 2 data set of a set frequency.
import rosbag
import numpy as np
import math
import matplotlib.pyplot as plt
import cv_bridge
import sys
from PIL import Image

files = ["2021-08-05-16-56-33.bag","2021-08-27-13-07-51.bag","oculus_2021-07-13-20-25-24.bag","oculus_2021-07-13-19-30-56.bag","2021-08-27-13-44-29.bag","2021-08-05-16-51-38.bag","oculus_2021-07-13-20-29-29.bag"]

# Plot Shape
fig, ax = plt.subplots(2, 4, figsize=(16, 12), constrained_layout = True)

title =  "HF vs LF Frequency Across Data\n"  + "Sonar Image in Azimuth(°) vs Range(m)\n"
fig.suptitle(title, fontsize=18)

sonar_plot_count = 0
for n in np.arange(len(files)):
    # Subplot postion
    x_s = math.floor(sonar_plot_count / 4)
    y_s = sonar_plot_count % 4

    # Open Data Set
    print(files[n])
    file_name = '/media/dberezansky/daniel_data/2021-07-13_sonar_target_size/' + files[n]
    bag = rosbag.Bag(open(file_name, 'rb'))
    for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
        if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
            break

    # Data gathering
    data = [int(foo) for foo in message.intensities]
    data = np.reshape(data, (-1,256))

    # View Sonar Image
    angles = [np.degrees(rad) for rad in message.azimuth_angles]
    ax[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")

    # Label Subplot
    # ax[x_s, y_s].set_xlabel('Azimuth (°)')
    # ax[x_s, y_s].set_ylabel('Range (m)')
    ax[x_s, y_s].tick_params(axis='x', which='both', labelsize=12)
    ax[x_s, y_s].tick_params(axis='y', which='both', labelsize=12)

    ax[x_s, y_s].set_aspect("auto")

    if message.frequency > 2000000:
        frequency = "High Frequency\n"
    else:
        frequency = "Low Frequency\n"
    subplotTitle = "T: " + files[n]
    ax[x_s, y_s].set_title(subplotTitle, fontsize=14)

    # Update Subplot
    sonar_plot_count += 1

# Label Data Sets
ax[0, 0].set_ylabel("High Frequency\n", fontsize=18)
ax[1, 0].set_ylabel("Low Frequency\n", fontsize=18)

ax[1, 3].axis('off')

# Save Plot
save_title = "FrequencyTestPlot.png"
fig.savefig(save_title)

# Show Plot
# plt.show()
im = Image.open(save_title)
im.show()
