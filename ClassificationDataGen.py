import rosbag
import numpy as np
import sys
import pickle

# File Update
path = sys.argv[1]
bag = rosbag.Bag(open(path, 'rb'))
target_path = path.split("/")[-1]
target_file = target_path.split(".")[0]

start_string = "Starting - " + target_file
print(start_string)

# Frame Counter
frame_count = 0
for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
    if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
        frame_count += 1

# Remove dictionary and instead use matrix operations

# Initialize Data Set
data = [int(foo) for foo in message.intensities] # Gets matrix of message intensities.
data = np.reshape(data, (-1,256)) # Reshapes matrix to n columns.
nrows, ncols = data.shape
data_set = np.zeros((nrows, ncols, frame_count))

# Generate Data Set
frame_index = 0
for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
    if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
        data = [int(foo) for foo in message.intensities] # Gets matrix of message intensities.
        data = np.reshape(data, (-1,256)) # Reshapes matrix to n columns.
        for r in np.arange(nrows):
            for c in np.arange(ncols):
                data_set[(r, c)][frame_index] = data[r][c]
        frame_index += 1
        progress = str(frame_index) + "/" + str(frame_count)
        print(progress)
        if frame_index > 400 - 1:
            break
print("Bag Files Loaded!\n")

# Initialize Result Matrices
means = np.zeros(ncols * nrows).reshape((nrows, ncols)) # Initializes means in shape of intensities matrix.
sigmas = np.zeros(ncols * nrows).reshape((nrows, ncols)) # Initializes sigmas in shape of intensities matrix.

# Compute Results
for r in np.arange(nrows): # Goes through each range (by row index).
    for c in np.arange(ncols): # Goes through each azimuth (by column index).
        means[r][c] = np.mean(data_set[(r,c)]) # Takes mean at given pixel point.
        sigmas[r][c] = np.std(data_set[(r,c)]) # Takes standard deviation at given pixel point.
    progress = str(r) + "/" + str(nrows - 1)
    print(progress)

# Data Serialization
meansName = 'MeanStore.' + target_file
meansFile = open(meansName, 'wb')
pickle.dump(means, meansFile)
meansFile.close()

sigmasName = 'SigmaStore.' + target_file
sigmasFile = open(sigmasName, 'wb')
pickle.dump(sigmas, sigmasFile)
sigmasFile.close()

# Success Output
exit_string = "Success - " + target_file
print(exit_string)