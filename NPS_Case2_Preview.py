# Program to preview a camera and sonar image for each NPS Case 2 data set of a set frequency.
import rosbag
import numpy as np
import math
import matplotlib.pyplot as plt
import cv_bridge
import sys
from PIL import Image

distance = ['30', '50', '70']
angle = ['0', '15', '30', '45']
frequency = sys.argv[1]

# Plot Shape
pcols = 8
prows = math.ceil((len(distance) * len(angle)) / (pcols / 2))
fig, ax = plt.subplots(prows, pcols, figsize=(3 * pcols, 4 * prows + 4), constrained_layout = True)

if frequency == 'HF':
    frequency_state = 'High'
elif frequency == 'LF':
    frequency_state = 'Low'
else:
    frequency_state = 'Error'
    print("Frequency incorrectly defined!")

title = 'NPS Experiment Case 2 ' + frequency_state + " Frequency Data Sets\n" + "Sonar Image in Azimuth(°) vs Range(m)"
fig.suptitle(title, fontsize=20)

sonar_plot_count = 0
camera_plot_count = 1
for d in np.arange(len(distance)):
    for a in np.arange(len(angle)):
        # Subplot postion
        x_s = math.floor(sonar_plot_count / pcols)
        y_s = sonar_plot_count % pcols
        x_c = math.floor(camera_plot_count / pcols)
        y_c = camera_plot_count % pcols

        # Open Data Set
        data_title = 'D' + distance[d] + '-A' + angle[a] + '-' + frequency
        data_set = 'NPS_Case2_H_' + data_title + '.bag'
        print(data_set)
        file_name = '/media/dberezansky/daniel_data/NPS_Case2/' + data_set
        bag = rosbag.Bag(open(file_name, 'rb'))
        for topic, message, timestamp in bag.read_messages(): # Iterate over data frames.
            if topic == '/raven/oculus/sonar_image': # Takes only sonar data frames.
                break

        # Data gathering
        data = [int(foo) for foo in message.intensities]
        data = np.reshape(data, (-1,256))

        # View Sonar Image
        angles = [np.degrees(rad) for rad in message.azimuth_angles]
        ax[x_s, y_s].imshow(data, extent=[np.min(angles), np.max(angles), 0, 2], aspect='auto', origin="lower")

        # View Camera Image
        for camera_topic, camera_message, camera_timestamp in bag.read_messages():
            if camera_topic == '/raven/stereo/right/image_raw':
                br = cv_bridge.CvBridge()
                camera_data = br.imgmsg_to_cv2(camera_message, "mono8")
                ax[x_c, y_c].imshow(camera_data, cmap="gray")
                break

        # Label Subplot
        # ax[x_s, y_s].set_xlabel('Azimuth (degrees)')
        # ax[x_s, y_s].set_ylabel('Range (meters)')

        # ax[x_c, y_c].set_title(data_title)
        ax[x_c, y_c].axis('off')
        ax[x_s, y_s].tick_params(axis='x', which='both', labelsize=14)
        ax[x_s, y_s].tick_params(axis='y', which='both', labelsize=14)
        ax[x_s, y_s].set_aspect("auto")
        ax[x_c, y_c].set_aspect("auto")

        # Update Subplot
        sonar_plot_count += 2
        camera_plot_count += 2

# Label Data Sets
for row in np.arange(len(distance)):
    row_label = "Distance " + distance[row]
    ax[row, 0].set_ylabel(row_label, fontsize=18)

col_index = 0
for col in np.arange(len(angle)):
    col_label = "Angle " + angle[col]
    ax[0, col_index].set_title(col_label, fontsize=18)
    col_index += 2

# Save Plot
save_title = "NPS_Case2_Preview-" + frequency + ".png"
fig.savefig(save_title)

# Show Plot
# plt.show()
im = Image.open(save_title)
im.show()
